# 1
list1 = [i for i in range(10)]
list2 = [i for i in range(5,15)]
list3 = [i for i in range(10,20)]
double = [*map(lambda x: x*2,list1)]
print(double)
x = [*map(lambda x,y,z: x*y*z,list1,list2,list3)]
print(x)
length = [*map(lambda x: len(str(x)),x)]
print(length)
f1 = [*filter(lambda x: x%2==0,list1)]
print(f1)
l = ['a','','','b','c','']
f2 = [*filter(None,l)]
print(f2)
z1 = [*zip(list1,list2,list3)]
print(z1)
z2 = [*zip(list1,map(lambda x: x*2,list2))]
print(z2)

